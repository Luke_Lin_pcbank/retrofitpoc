package ca.pcfinancial.facetoface2

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.DataOutputStream
import java.lang.String
import java.net.HttpURLConnection
import java.net.URL


class MainActivity : AppCompatActivity() {

    val TAG = "MainActivityLog"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.uat.pcfinancial.ca")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service: RestInterface = retrofit.create<RestInterface>(RestInterface::class.java)

        retrofit_post_btn.setOnClickListener {
            status_tv.text = "Ready"
            service.validateEmail(ValidateEmailRequest("Luke.Retrofit@gmail.com")).enqueue(object: Callback<JsonObject>{
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    status_tv.text = "onFailure"
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    status_tv.text = "onResponse"
                }

            })
        }

        regular_post_btn.setOnClickListener {
            status_tv.text = "Ready"
            sendPost()
        }

        gc_btn.setOnClickListener {
            Runtime.getRuntime().runFinalization()
        }
    }
    @SuppressLint("StaticFieldLeak")
    fun sendPost() {
        object: AsyncTask<Unit, Unit, Unit>(){
            var success = 0
            override fun doInBackground(vararg params: Unit?) {
                try {
                    val url = URL("https://api.uat.pcfinancial.ca/inet/banking/v2.0/customer/profile/email/validation")
                    val conn: HttpURLConnection = url.openConnection() as HttpURLConnection
                    conn.requestMethod = "POST"
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                    conn.setRequestProperty("Accept", "application/json")
                    conn.doOutput = true
                    conn.doInput = true
                    val jsonParam = ValidateEmailRequest("Luke@Regular@gmail.com")
                    Log.i(TAG, jsonParam.toString())
                    val os = DataOutputStream(conn.outputStream)
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString())
                    os.flush()
                    os.close()
                    Log.i(TAG, String.valueOf(conn.responseCode))
                    Log.i(TAG, conn.responseMessage)
                    success = conn.responseCode
                    conn.disconnect()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)
                status_tv.text = "Regular Call response: $success"
            }
        }.execute()
    }
}