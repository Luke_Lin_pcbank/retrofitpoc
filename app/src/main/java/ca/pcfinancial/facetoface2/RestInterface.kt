package ca.pcfinancial.facetoface2

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface RestInterface {

    @Headers("Andr_Req_Id" + ":" + "REQUEST_ID_VALIDATE_EMAIL")
    @POST("/inet/banking/v2.0/customer/profile/email/validation")
    fun validateEmail(@Body validateEmailRequest: ValidateEmailRequest?): Call<JsonObject>
}