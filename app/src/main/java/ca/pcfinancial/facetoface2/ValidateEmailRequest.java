package ca.pcfinancial.facetoface2;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ValidateEmailRequest implements Serializable {
    @SerializedName("address")
    String emailId;

    public ValidateEmailRequest(String emailId) {
        this.emailId = emailId;
    }
}
